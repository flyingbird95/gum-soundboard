import {Linking, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Colors from './Colors';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Sound from 'react-native-sound';

export default class Tile extends Component {
  handleClick() {
    if (typeof this.props.item.url !== 'undefined') {
      this.openLink();
    } else {
      this.playSound();
    }
  }

  playSound() {
    Sound.setCategory('SoloAmbient');
    let sound = new Sound(this.props.item.sound, Sound.MAIN_BUNDLE, error => {
      if (error) {
        console.log('failed to load the sound', error);
        return;
      }
      // loaded successfully
      console.log('duration in seconds: ' + sound.getDuration());
      console.log('number of channels: ' + sound.getNumberOfChannels());

      // Play the sound with an onEnd callback
      sound.play(success => {
        if (success) {
          console.log('successfully finished playing');
        } else {
          console.log('playback failed due to audio decoding errors');
        }
      });
    });
  }

  openLink() {
    Linking.canOpenURL(this.props.item.url).then(supported => {
      if (supported) {
        Linking.openURL(this.props.item.url);
      } else {
        console.log("Don't know how to open URI: " + this.props.item.url);
      }
    });
  }

  render() {
    return (
      <TouchableOpacity style={styles.item} onPress={() => this.handleClick()}>
        <View style={styles.content}>
          <Text allowFontScaling style={styles.text}>
            {this.props.item.text}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

let styles = StyleSheet.create({
  item: {
    flex: 1,
    alignSelf: 'stretch',
    padding: 16,
  },
  content: {
    flex: 1,
    backgroundColor: Colors.lighter,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  text: {
    color: Colors.dark,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});

Tile.propTypes = {
  text: PropTypes.string,
  sound: PropTypes.string,
};
