'use strict';
import Colors from './Colors';
import type {Node} from 'react';
import React from 'react';
import {ImageBackground, StyleSheet} from 'react-native';

const Header = (): Node => (
  <ImageBackground
    accessibilityRole={'image'}
    source={require('./logo.png')}
    style={styles.background}
    imageStyle={styles.logo}
  />
);

const styles = StyleSheet.create({
  background: {
    paddingBottom: 40,
    paddingTop: 164,
    paddingHorizontal: 32,
    backgroundColor: Colors.lighter,
  },
  logo: {
    margin: 20,
    flex: 1,
    resizeMode: 'contain',
  },
});

export default Header;
