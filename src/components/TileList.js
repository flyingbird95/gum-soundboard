'use strict';
import type {Node} from 'react';
import React from 'react';
import SquareGrid from 'react-native-square-grid';
import Tile from './Tile';

const URL =
  'https://script.google.com/a/groeneuilenmoestasj.nl/macros/s/AKfycbyJXYIdOWPWvYk3RclE-IZ8CbyNsvUQ_EJvFQaP70zWHwZ3fQg/exec';

let DATA = [
  {text: 'Uilenlied', sound: 'uilenlied.mp3'},
  {text: 'Deze club', sound: 'dezeclub.mp3'},
  {text: 'Bavtrick', sound: 'bavtrick.mp3'},
  {text: 'Bang Bang', sound: 'bangbangstevenadams.mp3'},
  {text: 'Bingo', sound: 'bingo.mp3'},
  {text: 'I like that', sound: 'draylike.mp3'},
  {text: 'Hand down, man down', sound: 'handdownmandown.mp3'},
  {text: 'Hell nah', sound: 'hellnahkg.mp3'},
  {text: 'His feelings got hurt', sound: 'klayfeelings.mp3'},
  {text: "Let's go", sound: 'lebronletsgo.mp3'},
  {text: 'Too easy', sound: 'lebrontooeasy.mp3'},
  {text: 'JaVale McGee', sound: 'mcgeeshaqtin.mp3'},
  {text: 'I like my meatball spicy', sound: 'meatballsspicy.mp3'},
  {text: 'Motherfocker', sound: 'melomf.mp3'},
  {text: '3 Pointer, Bang', sound: 'mikebreenbang.mp3'},
  {text: 'What a play', sound: 'ohmeohmysound.mp3'},
  {text: 'Oh yeah', sound: 'pgohyeah.mp3'},
  {text: 'Take that for data', sound: 'takethatfordata.mp3'},
  {text: 'The lob, the jam', sound: 'thelobthejam.mp3'},
  {text: 'There goes that man', sound: 'theregoesthatman.mp3'},
  {text: 'What', sound: 'westbrookwhat.mp3'},
  {text: 'You da real MVP', sound: 'youre_the_real_mvp.mp3'},
  {text: 'Ha Ha Ha Ha', sound: 'ha_ha_ha_ha.mp3'},
  {text: 'Upload your own sound', url: URL},
];

const TileList = (): Node => (
  <SquareGrid rows={0} columns={3} items={DATA} renderItem={renderItem} />
);

function renderItem(item) {
  return <Tile item={item} />;
}

export default TileList;
