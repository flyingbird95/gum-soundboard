# GUM-Soundboard
A soundboard for Groene-Uilen Moestasj, written in React-native.

To install, use the following command

```
npm i
```

## Debugging
To debug on Android, connect your phone to the laptop and use the following command:
```
react-native run-android
```

Extra documentation: https://facebook.github.io/react-native/docs/running-on-device

## Production
If you want to generate the production APK-file, run the following command:

*Make sure that you're in the `android`-folder:* `$ cd android`

```
./gradlew bundleRelease
```

Extra documentation: https://facebook.github.io/react-native/docs/signed-apk-android
Production for iOS: https://www.christianengvall.se/react-native-build-for-ios-app-store/


## Adding sounds
If you want to add new tiles, perform the following steps:
- Add a new row to the `DATA`-list, defined in `src/components/TileList.js`.
- Put the sound file in `android/app/src/main/res/raw`.
- For iOS, open the `ios`-folder in Xcode and add files to `soundboard/soundboard` by right-clicking 
this folder and then `add "Files" to project`.

## TODO:
The following things still needs to be done:

- Add more sounds for iOS and Android.
- Play one sound at a time.
